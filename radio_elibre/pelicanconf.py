AUTHOR = 'Hugovksy'
SITENAME = 'Radio Elibre'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Argentina/Buenos_Aires'

DEFAULT_LANG = 'es'

FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

LINKS = (
        ('Memorias de un Técnico Ligero', 'https://tecnico-ligero.enlacepilar.com.ar'),
         )

SOCIAL = (('No soy social', '#'),
          ('Acá tampoco hay enlaces', '#'),)

DEFAULT_PAGINATION = 8

DISQUS_SITENAME = "blog-elibre"

#en Huayra
#THEME = "/media/DATOS/Ubuntu-Huayra---Casa--cueva/Pelican/Temas-que-me-gustaron/ELIBRE-tema-bootlex"

#en Lenovo
THEME = "/home/enlacepilar/Huayra -- Ubuntu Casa -- Trabajo/Pelican/Temas-que-me-gustaron/ELIBRE-tema-bootlex/"