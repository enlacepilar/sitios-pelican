Title: ¿Qué onda esta página?
Date: 2022-07-11 20:24
Autor: Hugovksy
Slug: acerca-de-mi-o-que-onda
lang: es

Está bueno saber cuando uno navega por las páginas de internet de por ahí, de qué va esta web, sobre todo las personales como los blogs, ¿no? Capaz sí. Bueno, este blog intenta reunir mis experiencias como... bla bla bla. Nada de eso.

Acá va lo que me gusta:

* Me gusta ser libre
* Me gusta lo atemporal
* Me gusta lo que es sereno
* Me gusta multiplicar lo poco, por ende, me gusta el Tao (¿o era al revés?)
* Me gusta buscar el camino de retorno.
* Me gusta volver a ser niño.
* Me gusta el Aikido
* Me gusta el Software Libre
* Me gusta, de vez en cuando, dibujar, ¡como cuando hacía historietas a mano de chico!
* Me gusta leer.


##Mi contacto por si me querés escribir (no hay formularios ni nada):
[correo](mailto:enlacepilar@protonmail.com)
