Title: San Bernardo en Mayo
Date: 2024-05-30 19:46:55.629467
Autor: Hugovksy 
Slug: san-bernardo-mayo
Lang: es
Tags: San Bernardo, Buenos Aires
Category: San Bernardo
Imagen: /images/portada-san_bernardo04-mini.jpg



<div class='text-center mb-3'>
<img src='/images/san_bernardo04-mini.jpg' alt='foto' class='img-fluid'>
</div>

<div class='text-center mb-3'>
<img src='/images/san_bernardo03-mini.jpg' alt='foto' class='img-fluid'>
</div>

<div class='text-center mb-3'>
<img src='/images/san_bernardo02-mini.jpg' alt='foto' class='img-fluid'>
</div>
