Title: Títulos preventivos o un modo de presentación
Date: 2024-01-06 11:48:41.482065
Autor: Hugovksy (esta vez)
Slug: titulos-preventivos-presentacion
Lang: es

Esta página no pretende nada. Puede ser que lo hice como reto a mí para demostrarme que pude hacer un script con Python para que a su vez:

* Reduzca unas fotos al tamaño que le asigne y les cambie el nombre, incluyendo la primera como portada.

* Lo publique como sitio estático con el generador de sitios Pelican, de Python.

* Suba las fotos redimensionadas a la Raspberry y el archivo de la publicación en formato Markdown.

* Borre todas las fotos del directorio donde laburé con el script.

* Todo eso en forma automática, excepto los títulos, categorías y etiquetas.


Hasta ahora todo eso va andando. 

Después, el contenido son algunas fotos seleccionadas al azar, más o menos, de los paisajes que pude ver de primera mano, sacadas con el celu. 
Banco el culto a: [En Defensa de la Imagen Pobre, Hito Steyerl ](https://biblioteca-enlacelibre.duckdns.org/media/libros/en-defensa-de-la-imagen-pobre.pdf), algo que deberías leer si te gusta la imagen de poca calidad.

En fin, por el momento eso es todo.
