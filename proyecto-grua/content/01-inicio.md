Title: Proyecto Grúa
Date: 2022-09-13 18:34
Autor: Juan Torres
Slug: inicio-proyecto-grua
Lang: es
Tags: grua, automatizacion, radio control
Category: Proyecto
Summary: Descripción del incio del proyecto
Staus: published

Introducción
------------
El proyecto comenzó en una charla con [Nico](mailto:nicolas_alvarezmon@yahoo.com.ar), en la cual me comenta que quería/debía presentar un proyecto en la materia que está dando en la EEST nº 3 de Benavidez. La consigna general era agregar electrónica a algo existente utilizando [hardware libre](https://es.wikipedia.org/wiki/Hardware_libre "Referencia en Wikipedia").

Con esto en mente, empezamos a barajar posibilidades. Una de las primeras cosas que surgió fue automatizar un autito de control remoto que me donó la cuidadora de mi madre, el cual, si bien estaba entero, no funcionaba. Eesto implicaba un tiempo considerable en encontrar la falla, repararlo y adaptarlo, cosa que nos jugaba en contra, por lo cual decidimos dejarlo de lado (en esta ocasión, luego le daremos una segunda oportunidad) y seguir evaluando otras alternativas. También surgió la idea de automatizar algún electrodoméstico (p.ej. una tostadora, un lavarropas, etc.) pero también nos topamos con el problema del tiempo.

A la semana siguiente -comúnmente nos juntamos los domingos en casa- nos decidimos por el lado de los juguetes y seleccionamos los candidatos en base a posibilidades y gustos, de los cuales quedaron:
- Pala mecánica a radiocontrol
- Jeep a radiocontrol
- Grúa a radiocontrol

Finalmente Nico se decidió por esta última opción (motivado quizás por un proyecto inconcluso que quedó en la escuela donde trabaja y al cual no tiene acceso). Cuando empezamos a buscar, nos encontramos con que existen varios tipos de grúa [(ver clasificación)](https://www.gruasyequiposgarcia.com/clasificacion-tipo-y-caracteristicas-de-las-gruas/ "Referencia Grúas y Equipos García"). Primero Nico se interesó por las tipo torre (como las que se utilizan en la construcción) las cuales tienen 3 grados de libertad. Buscando apareció esta [grúa de construcción a control remoto](https://articulo.mercadolibre.com.ar/MLA-877935389-grua-de-construccion-a-radio-control-con-accesorios-a-pila-_JM "Link en Mercado Libre") a un precio tentador, pero había gato encerrado... si bien tiene 3 grados de libertad, el movimiento longitudinal del carro era manual, cosa que no estaba especificada en el producto. Devolución mediante, vimos una con los 3 movimientos manejados por control remoto, pero el precio era elevado y se iba del presupuesto. Esto nos llevó a esta otra [grúa a control remoto](https://articulo.mercadolibre.com.ar/MLA-869953279-grua-a-radio-control-_JM "Link en Mercado Libre") la cual si bien ofrece movimiento en sólo en 2 dimensiones, estaba dentro del presupuesto y, en definitiva, cumplía con la premisa original, así que fuimos por ella y comenzamos a emprender el proyecto.


<a href="images/01.jpg" target="_blank"><img src= "images/01_thumb.jpg"/></a><br>

<a href="images/02.jpg" target="_blank"><img src= "images/02_thumb.jpg"/></a><br>

<a href="images/03.jpg" target="_blank"><img src= "images/03_thumb.jpg"/></a><br>

<a href="images/04.jpg" target="_blank"><img src= "images/04_thumb.jpg"/></a><br>

<a href="images/05.jpg" target="_blank"><img src= "images/05_thumb.jpg"/></a><br>

<a href="images/06.jpg" target="_blank"><img src= "images/06_thumb.jpg"/></a><br>

<a href="images/07.jpg" target="_blank"><img src= "images/07_thumb.jpg"/></a><br>

<a href="images/08.jpg" target="_blank"><img src= "images/08_thumb.jpg"/></a><br>

<a href="images/09.jpg" target="_blank"><img src= "images/09_thumb.jpg"/></a><br>

<a href="images/10.jpg" target="_blank"><img src= "images/10_thumb.jpg"/></a><br>

<a href="images/11.jpg" target="_blank"><img src= "images/11_thumb.jpg"/></a><br>

<a href="images/12.jpg" target="_blank"><img src= "images/12_thumb.jpg"/></a><br>

<a href="images/13.jpg" target="_blank"><img src= "images/13_thumb.jpg"/></a><br>










