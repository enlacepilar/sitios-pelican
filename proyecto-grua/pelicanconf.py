AUTHOR = 'Proyecto Grúa - Nico y JCT'
SITENAME = 'Proyecto Grúa'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Argentina/Buenos_Aires'

DEFAULT_LANG = 'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

LINKS = (('Biblioteca EnlaceLibre', 'https://biblioteca-enlacelibre.duckdns.org/'),
         ('Ocruxaves', 'https://ocruxaves.com.ar'),
         ('Biblioteca de pruebas en django', 'https://biblioteca-enlacelibre.enlacepilar.com.ar'),
         ('Radio Elibre Guerrilla', 'https://elibre.netlify.app/'),)

SOCIAL = (('No soy social', '#'),
          ('Acá tampoco hay enlaces', '#'),)
          

DEFAULT_PAGINATION = 8

#DISQUS_SITENAME = "memorias-de-un-tecnico-ligero"

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = "/media/DATOS/Ubuntu-Huayra---Casa--cueva/Pelican/Temas-que-me-gustaron/tecnico-ligero-brutalist"

