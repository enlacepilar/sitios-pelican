Introducción
------------
El proyecto comenzó en una charla con Nico (<nicolas_alvarezmon<@yahoo.com.ar>), en la cual me comenta que quería/debía presentar un proyecto en la materia que está dando en la EEST nº 3 de Benavidez. La consigna general era agregar electrónica a algo existente utilizando [hardware libre](https://es.wikipedia.org/wiki/Hardware_libre "Referencia en Wikipedia").

Con esto en mente, empezamos a barajar posibilidades. Una de las primeras cosas que surgió fue automatizar un autito de control remoto que me donó la cuidadora de mi madre, el cual, si bien estaba entero, no funcionaba. Eesto implicaba un tiempo considerable en encontrar la falla, repararlo y adaptarlo, cosa que nos jugaba en contra, por lo cual decidimos dejarlo de lado (en esta ocasión, luego le daremos una segunda oportunidad) y seguir evaluando otras alternativas. También surgió la idea de automatizar algún electrodoméstico (p.ej. una tostadora, un lavarropas, etc.) pero también nos topamos con el problema del tiempo.

A la semana siguiente -comúnmente nos juntamos los domingos en casa- nos decidimos por el lado de los juguetes y seleccionamos los candidatos en base a posibilidades y gustos, de los cuales quedaron:
- Pala mecánica a radiocontrol
- Jeep a radiocontrol
- Grúa a radiocontrol

Finalmente Nico se decidió por esta última opción (motivado quizás por un proyecto inconcluso que quedó en la escuela donde trabaja y al cual no tiene acceso). Cuando empezamos a buscar, nos encontramos con que existen varios tipos de grúa [(ver clasificación)](https://www.gruasyequiposgarcia.com/clasificacion-tipo-y-caracteristicas-de-las-gruas/ "Referencia Grúas y Equipos García"). Primero Nico se interesó por las tipo torre (como las que se utilizan en la construcción) las cuales tienen 3 grados de libertad. Buscando apareció esta [grúa de construcción a control remoto](https://articulo.mercadolibre.com.ar/MLA-877935389-grua-de-construccion-a-radio-control-con-accesorios-a-pila-_JM "Link en Mercado Libre") a un precio tentador, pero había gato encerrado... si bien tiene 3 grados de libertad, el movimiento longitudinal del carro era manual, cosa que no estaba especificada en el producto. Devolución mediante, vimos una con los 3 movimientos manejados por control remoto, pero el precio era elevado y se iba del presupuesto. Esto nos llevó a esta otra [grúa a control remoto](https://articulo.mercadolibre.com.ar/MLA-869953279-grua-a-radio-control-_JM "Link en Mercado Libre") la cual si bien ofrece movimiento en sólo en 2 dimensiones, estaba dentro del presupuesto y, en definitiva, cumplía con la premisa original, así que fuimos por ella y comenzamos a emprender el proyecto.


Basic styles
------------
With this markup you can obtain *simple emhpasis* (usually rendered in italic text), **strong emphasis** (usually rendered in bold text), `source code` text (usually rendered in monospaced text), or ~~strikethrough~~ text (usually rendered with a line through text).

You may use also _this_ or __this__ notation to emphatize text, and you can use all them _**`together`**_ (and you can mix `*` and `_` )

If you look at the source code you may note that
even 
if 
you 
break 
the 
lines,
the text is kept together
in a single paragraph

 Paragraphs are delimited by blank lines, leading and trailing spaces are removed 

You may force a line break with two spaces  
or with a `\`\
at the end of

Links
-----
- You can insert links in text like [this](/tutorial.md)

- You may add a [title](https://agea.github.io/tutorial.md "Markdown Tutorial") to your link (can you see the tooltip?)

- If your link contains spaces you have to write the [link](<http://example.com/a space>) between `<>`

- You can use spaces and markup inside the [link **text**](https://agea.github.io/tutorial.md)

- Long links may decrease source readability, so it's posible to define all links somewhere in the document (the end is a good place) and just reference the [link][tutorial.md], you may also collapse the reference if it matches the link text (example:  [tutorial.md][])

- You may also write directly the link: <https://agea.github.io/tutorial.md>

- It will work also for email addresses: <email@example.com> (you may write vaild email links also using [mailto](mailto:email@example.com) as protocol)


[tutorial.md]: https://agea