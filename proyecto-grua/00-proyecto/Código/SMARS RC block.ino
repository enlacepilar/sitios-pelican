#include <AFMotor.h>

AF_DCMotor MotorL(1);   // Motor for drive Left on M1
AF_DCMotor MotorR(2);   // Motor for drive Right on M2

String readString, action, Lspeed, Rspeed, actionDelay, stopDelay;  // declaring multiple strings

void setup(){
  Serial.begin(115200);    // set up Serial library at 115200 bps
  Serial.println("SMARS Command Block Mod");
  
  // Set the speed to start, from 0 (off) to 255 (max speed)
  // sometimes the motors don't have the same speed, so use these values tomake your SMARS move straight
  MotorL.setSpeed(255);
  MotorR.setSpeed(255);
  // turn off motor
  MotorL.run(RELEASE);
  MotorR.run(RELEASE);
}

void loop() {
  while (Serial.available() > 0){
     char c = Serial.read();    // gets one byte from serial buffer
     readString += c;
     
     if (c == '\n') {
         Serial.println("---------------");
         Serial.print(readString);    // prints string to serial port out

         int n1;   
         int n2; 
         int n3; 
         int n4; 
         
         action = readString.substring(0, 1);     // get a substring of a String
         if(action.equals("S")){
           stopDelay = readString.substring(1, 6);    

           Serial.println(action); 
           Serial.println(stopDelay);

           char carray4[6];                                  // declaring character array
           stopDelay.toCharArray(carray4, sizeof(carray4));  // passing the value of the string to the character array
           n4 = atoi(carray4);                               // convert char/string to a integer value
           
           Serial.println(n4);  
           
         } else {
           // separate the string that receive from serial buffer into several string
           Lspeed = readString.substring(1, 4);
           Rspeed = readString.substring(4, 7);
           actionDelay = readString.substring(7, 12);

           Serial.println(action); 
           Serial.println(Lspeed);
           Serial.println(Rspeed);
           Serial.println(actionDelay);

           char carray1[12];                                // declaring character array
           Lspeed.toCharArray(carray1, sizeof(carray1));    // passing the value of the string to the character array
           n1 = atoi(carray1);                              // convert char/string to a integer value
           
           char carray2[12];
           Rspeed.toCharArray(carray2, sizeof(carray2));
           n2 = atoi(carray2);
           
           char carray3[12];
           actionDelay.toCharArray(carray3, sizeof(carray3));
           n3 = atoi(carray3);
           

           Serial.println(n1);    // prints integer value n1 to serial port out
           Serial.println(n2);    // prints integer value n2 to serial port out 
           Serial.println(n3);    // prints integer value n3 to serial port out
         }

         readString = "";     // clears variable for new input
       

       // move forward
       if(action.equals("F")){
            MotorL.setSpeed(n1);
            MotorR.setSpeed(n2);
            MotorL.run(FORWARD);
            MotorR.run(FORWARD);
            delay(n3);            
            MotorL.run(RELEASE);
            MotorR.run(RELEASE);

       // turn left
       } else if(action.equals("L")){
            MotorL.setSpeed(n1);
            MotorR.setSpeed(n2);
            MotorL.run(BACKWARD);
            MotorR.run(FORWARD);
            delay(n3);            
            MotorL.run(RELEASE);
            MotorR.run(RELEASE);

       // turn right
       } else if(action.equals("R")){
            MotorL.setSpeed(n1);
            MotorR.setSpeed(n2);
            MotorL.run(FORWARD);
            MotorR.run(BACKWARD);
            delay(n3);           
            MotorL.run(RELEASE);
            MotorR.run(RELEASE);

       // stop/not move
       } else if(action.equals("S")){
            delay(n4);            
       }
     }   
  } 
}