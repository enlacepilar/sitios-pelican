#include <SoftwareSerial.h>

#define DETENIDO 13
#define FDC  12
#define B_1B 11
#define B_1A 10
#define A_1B 9
#define A_1A 8
#define BUZZ 7
#define ECHO 6
#define TRIG 5
#define TX   4
#define RX   3
#define PISO 32 // distancia [cm] al Piso

bool Piso, FinDeCarrera;
bool Bajando = false;
bool Subiendo = false;
bool GirandoCW = false;
bool GirandoCCW = false;
bool Detenido = true;
char C, Comando;
String BufferRx;
SoftwareSerial BT(RX, TX);

void setup() {
  BT.begin (115200); // HC06
  Serial.begin (115200);
  Serial.println ("Grua BT v1.0");
  pinMode (DETENIDO, OUTPUT);
  pinMode (FDC, INPUT_PULLUP);
  pinMode (ECHO, INPUT);
  pinMode (TRIG, OUTPUT);
  pinMode (BUZZ, OUTPUT);
  pinMode (A_1A, OUTPUT); 
  pinMode (A_1B, OUTPUT);  
  pinMode (B_1A, OUTPUT);   
  pinMode (B_1B, OUTPUT);
  digitalWrite (TRIG, LOW); // inicializo sensor ultrasonido
}

void loop() {
  // chequeo fines de carrera
  (!digitalRead(FDC))? FinDeCarrera = true: FinDeCarrera = false;
  (distancia() < PISO)? Piso = true: Piso = false;

  if (Bajando && Piso) {
    Detenido = true;
    Bajando = false;
  }

  if (Subiendo && FinDeCarrera) {
    Detenido = true;
    Subiendo = false;
  }

  if (Detenido) {
    digitalWrite (A_1A, LOW); 
    digitalWrite (A_1B, LOW);
    digitalWrite (B_1A, LOW); 
    digitalWrite (B_1B, LOW);
    Subiendo = false;
    Bajando = false;
    GirandoCCW = false;
    GirandoCW = false;
    digitalWrite(DETENIDO, HIGH);
  }
  else {
    digitalWrite(DETENIDO, LOW);
    if (Subiendo) {
      digitalWrite (B_1A, HIGH);
      digitalWrite (B_1B, LOW);
    }

    if (Bajando) {
      digitalWrite (B_1A, LOW);
      digitalWrite (B_1B, HIGH);
    }

    if (GirandoCW) {
      digitalWrite (A_1B, LOW);
      digitalWrite (A_1A, HIGH);      
    }

    if (GirandoCCW) {
      digitalWrite (A_1B, HIGH);
      digitalWrite (A_1A, LOW);
    }
  }

  // veo si recibi algun comando
  if (BT.available() > 0) {
    C = BT.read();
    BufferRx += C;
    Comando = 'X'; // limpio el último comando

    if (C == '\n'){
      Comando = BufferRx.charAt(0); // leo comando recibido
      BufferRx = ""; // limpio el buffer
    }

    switch (Comando) {
    // sube
    case 'F':
      Detenido = false;
      Subiendo = true;
      Bajando = false;
      break;
    // baja
    case 'B':
      Detenido = false;
      Bajando = true;
      Subiendo = false;
      break;
    // giro izquierda (CCW)
    case 'L':
      Detenido = false;
      GirandoCCW = true;
      GirandoCW = false;
      break;
    // giro derecha (CW)
    case 'R':
      Detenido = false;
      GirandoCW = true;
      GirandoCCW = false;
      break;
    // parar (ambos motores)
    case 'S':
      Detenido = true;
      break;
    // bocina
    case 'H':
      digitalWrite(BUZZ, HIGH);
      delay(150);        
      digitalWrite(BUZZ, LOW);
      delay(100);        
      digitalWrite(BUZZ, HIGH);
      delay(250);        
      digitalWrite(BUZZ, LOW);
      break;
    }
  }
}

long distancia()
{
  long t = 0;
  for (byte i = 0; i < 3; i++) {
    digitalWrite (TRIG, HIGH);
    delayMicroseconds (10); // pulso de 10us
    digitalWrite (TRIG, LOW);
    t += pulseIn (ECHO, HIGH);  // obtenemos el ancho del pulso 
  }
  
  // promedio de las 3 lecturas y conviersion a cm
  return t / 177; // formula original: t/59
}