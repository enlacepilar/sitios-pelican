#include <AFMotor.h>   
#include <Servo.h> 

AF_DCMotor MotorL(1);  // Motor for drive Left on M1
AF_DCMotor MotorR(2);  // Motor for drive Right on M2

const int buzPin = 2;  // set pin 2 as buzzer pin (use active buzzer)
Servo servo1;          // create a servo object servo1
int valServo;

String readString, action, Lspeed, Rspeed, actionDelay, stopDelay;  // declaring multiple strings

void setup(){
  Serial.begin(115200);  // set up Serial library at 115200 bps
  Serial.println("SMARS Drawer/Shovel Mod");

  pinMode(buzPin, OUTPUT);  // sets the buzzer pin as an Output
  servo1.attach(10);        // tell the servo1 object that its servo is plugged into pin 10
  servo1.write(0);          // move the servo to the 0 degree position
  
  // Set the speed to start, from 0 (off) to 255 (max speed)
  // sometimes the motors don't have the same speed, so use these values tomake your SMARS move straight
  MotorL.setSpeed(255);
  MotorR.setSpeed(255);
  // turn off motor
  MotorL.run(RELEASE);
  MotorR.run(RELEASE);
}

void loop() {
  while (Serial.available() > 0) {
     char c = Serial.read();    // gets one byte from serial buffer 
     readString += c;
     
     if (c == '\n') {
         Serial.println("---------------");
         Serial.print(readString);    // prints string to serial port out

         int n1;   
         int n2; 

         // separate the string that receive from serial buffer into several substring
         action = readString.substring(0, 1);
         Lspeed = readString.substring(1, 4);
         Rspeed = readString.substring(4, 7);

         Serial.println(action); 
         Serial.println(Lspeed);
         Serial.println(Rspeed);

         char carray1[7];                                 // declaring character array
         Lspeed.toCharArray(carray1, sizeof(carray1));    // passing the value of the string to the character array
         n1 = atoi(carray1);                              // convert char/string to a integer value
         
         char carray2[7];
         Rspeed.toCharArray(carray2, sizeof(carray2));
         n2 = atoi(carray2);

        
         Serial.println(n1);    // prints integer value n1 to serial port out
         Serial.println(n2);    // prints integer value n2 to serial port out  
         
         readString = "";
       
       // move forward
       if(action.equals("F")){
            MotorL.setSpeed(n1);
            MotorR.setSpeed(n2);
            MotorL.run(FORWARD);
            MotorR.run(FORWARD);

       // move backward
       } else if(action.equals("B")){
            MotorL.setSpeed(n1);
            MotorR.setSpeed(n2);
            MotorL.run(BACKWARD);
            MotorR.run(BACKWARD);

       // turn left
       } else if(action.equals("L")){
            MotorL.setSpeed(n1);
            MotorR.setSpeed(n2);
            MotorL.run(BACKWARD);
            MotorR.run(FORWARD);

       // turn right
       } else if(action.equals("R")){
            MotorL.setSpeed(n1);
            MotorR.setSpeed(n2);
            MotorL.run(FORWARD);
            MotorR.run(BACKWARD);

       // stop
       } else if(action.equals("S")){
            MotorL.run(RELEASE);
            MotorR.run(RELEASE);

       // turn on horn
       } else if(action.equals("H")){
            digitalWrite(buzPin, HIGH);
            delay(120);        
            digitalWrite(buzPin, LOW);
            delay(100);        
            digitalWrite(buzPin, HIGH);
            delay(120);        
            digitalWrite(buzPin, LOW);

       // set position of servo
       } else if(action.equals("V")){
            servo1.write(n1);   // move the servo position depend on value n1
            delay(15);
       }
     }   
  }

  
}